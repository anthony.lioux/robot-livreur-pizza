<?php

/**
 * La classe RobotLivreurPizza décrit le comportement du tout nouveau robot livreur de pizzas de JBHuet.com !
 *
 * Le nouveau robot livreur de pizzas de JBHuet.com est une formidable machine qui va chercher pour vous la pizza qui sort du four :
 * votre pizza préférée vient à vous, sans que vous ayez à bouger.
 * En pleine session de codage intense, pas possible de vous lever pour récupérer la pizza alors que le four sonne ? Pas de souci !
 * Le robot livreur de pizzas de JBHuet.com vous l'apporte sans effort, ou presque.
 * Cette merveilleuse machine se programme très simplement grâce à sa classe qui décrit très précisément tout ce qu'il peut faire pour vous.
 * Il vous suffit d'utiliser cette classe pour décrire à votre robot comment vous apporter votre délicieuse pizza.
 *
 * Ce robot perfectionné est doté d'un écran pour afficher les informations dont vous pourriez avoir besoin.
 * Vous pouvez même lui faire vous souhaiter un bon appétit !
 * NB. : ce robot étant encore à l'état de prototype, l'écran fournit actuellement ne peut afficher que des messages de 255 caractères maximum.
 * Grâce à ses capteurs (testeur de pente dans le sens du déplacement, testeur de distance aux obstacles face à lui, testeur de position de la pizza par rapport au plateau)
 * le robot peut s'adapter à de nombreuses situations.
 *
 * @author Jean-Bernard HUET <contact@jbhuet.com>
 *
 * @version 1.0.0
 *
 * PS. : les robots livreurs de pizzas existent déjà https://www.youtube.com/watch?v=rb0nxQyv7RU et https://www.youtube.com/watch?v=mIwDhnPnb4o
 */
class RobotLivreurPizza {

    // Attributs privés
    private $NomDestinataire;
    private $PizzaPréférée;
    /** @var string $Message Le message affiché à l'écran. */
    private $MessageEcran = "";

    /** @var integer CAPA_AFFICH Constante représentant le nombre maximal de caractères affichés à l'écran */
    private const CAPA_AFFICH = 255;

    /**
     * Instance : RobotLivreurPizza - Constructeur
     *
     * Ce constructeur construit l'instance d'un objet de type RobotLivreurPizza
     *
     * @param string    $Nom    Le nom en toute lettre du destinataire de la pizza
     * @param string    $Pizza  Le type de pizza, 1 = 4 fromages, 2 = margherita
     */
    public function __construct(string $Nom, int $Pizza) {
        // J'ai mis ça là, mais à vous de voir si c'est utile...
        $this->NomDestinataire=$Nom;
        $this->PizzaPréférée=$this->ControlePizza( $Pizza );
    }

    // Méthodes publiques

    /**
     * Pour faire avancer le robot
     * @param float $Distance pour avancer, valeur > 0
     */
    public function Avancer( float $Distance ){
            if (0 < $Distance) {
                $this-> AfficherMessage(sprintf('J\'avance de %s mètres.<br>',($Distance)));
            } else {
                $Distance = 0;
                $this->AfficherMessage(sprintf('Valeur invalide.<br>'));
            }
        }

    /**
     * Pour faire reculer le robot
     * @param float $Distance pour reculer, valeur > 0
     */
    public function Reculer( float $Distance ){
        if (0 < $Distance) {
            $this-> AfficherMessage(sprintf('Je recule de %s mètres.<br>',($Distance)));
        } else {
            $Distance = 0;
            $this->AfficherMessage(sprintf('Valeur invalide.<br>'));
        }
    }



    /**
     * Pour faire tourner à droite le robot
     * @param int $Rotation pour tourner à droite, valeur doit être compris entre > 0 et 180 
     */
    public function TournerDroite( float $Rotation ) {
        // Le robot affiche le nombre de degrés vers la droite dont il tourne sur lui-même
        if (0 < $Rotation && 180 >= $Rotation) {
            $this->AfficherMessage(sprintf('Je tourne à droite de %s  °.<br>', ($Rotation)));
        } else {
            $Rotation = 0;
            $this->AfficherMessage(sprintf('Valeur invalide.<br>'));
        }

    }

    /**
     * Pour faire tourner à gauche le robot
     * @param int $Rotation pour tourner à gauche, valeur doit être compris entre > 0 et 180 
     */
    public function TournerGauche( float $Rotation ) {
        // Le robot affiche le nombre de degrés vers la droite dont il tourne sur lui-même
        if (0 < $Rotation && 180 >= $Rotation) {
            $this->AfficherMessage(sprintf('Je tourne à gauche de %s  °.<br>', ($Rotation)));
        } else {
            $Rotation = 0;
            $this->AfficherMessage(sprintf('Valeur invalide.<br>'));
        }

    }


    /**
     * Pour monter le plateau
     * @param int $Lever pour monter le plateau, la valeur maximale du plateau est de 90cm. La valeur initiale est de 50cm
     */
    public function MonterPlateau( int $Lever) {
        # Le robot possède un plateau pour transporter la pizza.
        # Au plus bas, le plateau est à 40cm au-dessus du sol.
        # Le plateau est fixé sur un verrin qui peut monter ou descendre.
        # Selon les modèles de robot, le verrin peut monter plus ou moins haut.
        # La hauteur maximale du verrin (donc du plateau) est fixe (une constante).
        # Je vous laisse libre de déterminer cette hauteur maximale.
        # Il faudra vérifier que le robot ne reçoit pas un ordre de monter le plateau en dehors de la plage (hauteur minimum / hauteur maximum) possible.
        # Le robot affiche la hauteur en centimètres à laquelle monte le plateau.
        if (40 <= $Lever && 95 >= $Lever) {
            $this->AfficherMessage(sprintf('Je monte le plateau à %s centimètres.<br>', ($Lever)));
        } else {
            $Lever > 95;
            $this->AfficherMessage(sprintf('Le plateau ne peut pas monter aussi haut<br>'));
        }
    }

    /**
     * Pour baisser le plateau
     * @param int $Baisser pour monter le plateau, la valeur maximale du plateau est de 90cm. La valeur initiale est de 50cm
     */
    public function DescendrePlateau( int $Baisser) {
        if (40 <= $Baisser && 95 >= $Baisser) {
            $this->AfficherMessage(sprintf('Je descends le plateau à %s centimètres.<br>', ($Baisser)));
        } else {
            $Baisser < 40;
            $this->AfficherMessage(sprintf('Le plateau ne peut pas descendre aussi bas<br>'));
        }
    }

    /**
     * Pour pousser la pizza hors du plateau
     * @param $Pousser pour pousser la pizza, true / pour ne pas pousser la pizza, false
     */
    public function PousserPizzaHorsPlateau(bool $Pousser) {
        /*
         * Au bord du plateau, le robot possède une spatule coudée qui se glisse sous la pizza pour la pousser hors du plateau.
         * Le robot affiche la confirmation qu'il a poussé la pizza hors du plateau.
         */
        if ($Pousser === true) {
            $this->AfficherMessage(sprintf('Je pousse la pizza hors du plateau, Bon appétit Maître !<br><br>'));
        } else {
            $this->AfficherMessage(sprintf('La pizza n\'a pas été poussé hors du plateau.<br><br>' ));
        }
    }

    /**
     * Pour tirer la pizza et la glisser sur le plateau
     * @param $Tirer pour tirer et glisser la pizza, true / pour ne pas tirer et glisser la pizza, false
     */
    public function TirerPizzaSurPlateau(bool $Tirer) {
        /*
         * À côté du plateau, une pince permet au robot d'attraper la pizza pour la glisser sur le plateau.
         * Le robot affiche un message de confirmation que la pizza a bien était tirée et est maintenant sur le plateau.
         */
        if ($Tirer === true) {
            $this->AfficherMessage(sprintf('Je tire la pizza et la glisse sur le plateau.<br>'));
        } else {
            $this->AfficherMessage(sprintf('La pizza n\'a pas été tiré et glissé sur le plateau.<br>' ));
        }

    }

    /**
     * Pour afficher le nom du destinataire de la pizza
     * @param string $NouveauNom
     */
    public function SetName(string $NouveauNom) {
        $this->NomDestinataire = $NouveauNom;
    }


    /**
     * Pour afficher le type de pizza
     * @param int $NewPizza
     * 
     */
    public function SetPizza(int $NewPizza) {
        $this->PizzaPréférée=$this->ControlePizza( $NewPizza );
    }

    private function ControlePizza( int $ValeurPizza ): int {
        if( ($ValeurPizza < 1) || ($ValeurPizza > 2) ) {
            $ValeurPizza = 1;
        }
        return $ValeurPizza;
    }

    public function Bonjour() {
        switch( $this->PizzaPréférée ) {
            case 1:
                $this->AfficherMessage(sprintf( PHP_EOL . "Bonjour %s !<br> Tu as bon goût.<br><br>",
                        $this->NomDestinataire ));
                break;
            case 2:
                $this->AfficherMessage(sprintf( PHP_EOL . "Bonjour %s !<br> Tu n'as pas bon goût.<br><br>",
                        $this->NomDestinataire ));
                break;
            default : break;
        }
    }


    /**
     * Pour afficher le message
     * @param string $Message
     */
    public function AfficherMessage( string $Message ) {
        $this->MessageEcran = $this->TesterLongueurMessage( $Message );
        print($this->MessageEcran );
    }

    // Méthodes privées
    /**
     * Vérifie que le message à afficher ne dépasse pas la capacité de l'écran
     *
     * La méthode privée TesterLongueurMessage teste si le message passé en paramètre dépasse la capacité d'affichage de l'écran.
     * Si le message dépasse la capacité de l'écran, tous les caractères au-delà de la capacité maximale de l'écran sont supprimés
     *    et "..." est ajouté à la fin du nouveau message.
     * NB. : la longueur maximale du nouveau message ("..." compris) ne peut pas dépasser la capacité d'affichage de l'écran.
     *
     * @link https://www.php.net/manual/fr/language.oop5.constants.php Pour comprendre la notation self::CAPA_AFFICH
     * @link https://www.php.net/manual/fr/function.strlen.php Pour savoir ce que fait la fonction PHP strlen
     *
     * @param string $MessagePossible Valeur du message avant réduction de la longueur si celle-ci dépasse la capacité de l'écran
     * @return string $Valeur du message après réduction éventuelle de la longueur
     */
    private function TesterLongueurMessage( string $MessagePossible ): string {
        if( self::CAPA_AFFICH < strlen( $MessagePossible ) ) {  // Vous pouvez modifier le code déja écrit...
            $MessagePossible = substr($MessagePossible, 0, self::CAPA_AFFICH);
    $last_space = strrpos($MessagePossible, " ");
    $MessagePossible = substr($MessagePossible, 0, $last_space)."...";
        }
        return $MessagePossible;
    }

}

// Ecrivez ci-dessous le code qui sera transmis à votre robot pour aller chercher votre pizza dans le four et l'apporter à votre bureau
// En imaginant que vous possédez ce robot, et que vous êtes installé·e à votre bureau, programmez le robot pour qu'il vous rapporte votre pizza toute chaude.
// Donnez des ordres au robot en fonction de la réalité de votre logement.
// Le robot peut partir de n'importe quel point (sous votre bureau, un placard, un coin de votre cuisine).
// On considère que la porte du four est ouverte, et qu'elle n'empêche pas le robot d'atteindre la pizza.
// Le robot doit déposer la pizza dans une assiette posée au bord de votre bureau.
// La dernière instruction que le robot devra exécuter est de vous souhaiter un bon appétit.

// Programme Principal

$RobotPizza1 = new RobotLivreurPizza("Anthony", 1);
$RobotPizza1 -> Bonjour();
$RobotPizza1 -> Avancer(10);
$RobotPizza1 -> TournerDroite(60);
$RobotPizza1 -> Avancer(3);
$RobotPizza1 -> TournerDroite(30);
$RobotPizza1 -> DescendrePlateau(40);
$RobotPizza1 -> TirerPizzaSurPlateau(true);
$RobotPizza1 -> TournerGauche(30);
$RobotPizza1 -> Reculer(3);
$RobotPizza1 -> TournerGauche(60);
$RobotPizza1 -> Reculer(10);
$RobotPizza1 -> MonterPlateau(80);
$RobotPizza1 -> PousserPizzaHorsPlateau(true);


$RobotPizza2 = new RobotLivreurPizza("Jean-Bernard", 2);
$RobotPizza2 -> Bonjour();